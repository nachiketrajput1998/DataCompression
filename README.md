Huffman algorithm is used.
The program is able to compress and decompress any file.
Program will run like this:

***first two commands are for compression and later two demonstrate decompression***

./program -c1  <file> <compressed-file-name>
./program -c2 <file> <compressed-file-name>
./program -uc1 <compressed-file> <uncompressed-file>
./program -uc2 <compressed-file> <uncompressed-file> 
