/* Use to read the data in file.
*/
#include <stdio.h>
#include <stdlib.h> 
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>
int main(int argc,char* argv[]){
	int fd;
	char ch;
	fd = open(argv[1],O_RDONLY);
	if(argc != 2){
		perror("Invalid argument");
		errno = EINVAL;
		return EINVAL;
	}
	if(fd == -1){
		perror("File not open successfully");
		errno = EINVAL;
		return errno;
	}
	while(read(fd,&ch,sizeof(char))){
		printf("%c",ch);
	}
	return 0;
}
