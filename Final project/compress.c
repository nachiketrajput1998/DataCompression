#include <stdio.h>
#include <stdlib.h> 
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include "list.h"
#define START 1
#define CHECK 2

int main(int argc , char *argv[]){
	float ratio;
	int flen , flen1;
	node *q , *ht, *st ,*st1;
	int fd , fd1 , i , j,count = 0,m = 0;
	char *tbuffer, targv[15] , ctargv[15];
	int choice = 0;
	char c , ch; 
	int size , chk, first = 1, len, a;
	size = 1024;
	char fnamet[15] , gzname[15];
	list l;
	init(&l);
	if(argc > 2){
		perror("Invalid argument.....");
		errno = EINVAL;
		return errno;
	}
	if(argc == 1){
		perror("Add two few arguments....");
		errno = EINVAL;
		return errno;
	}
	strcpy(fnamet , argv[1]);
	for(i = 0; fnamet[i] != '\0'; i++){
		gzname[i] = fnamet[i];
	}
	gzname[i] = '\0';
	strcat(gzname,".gz");
	fd=open(argv[1] , O_RDONLY);
	if(fd == -1){
		perror("File not open successfully.....");
		errno = EINVAL;
		return errno;
	}
/*Reads the file and append it to list character by character ,first character directly append to list
* after that it first searches whether it is repeated or not , if repeated then increment in frequency 
* otherwise call append() in search() .
*/		
	while(read(fd,&ch,sizeof(char))){
		if(first == START){
			append(&l , ch);
			first = CHECK;
		}
		else 
			search(&l , ch);
	}
	close(fd);
	q = l.head;
/* It return the no. of nodes */
	//count = no_of_node(q);
/* This function initializes the weight to initial value */
	initial_wt(q);
/* This function is used to break the list and then merge sort is used to sort the list */
	ht = bmerge(q);
/* The Huffman tree is constructed to using huffman algo so as the 
 * characters are the leaf nodes
*/
	st = huff_tree(ht);
	st1 = st;
/* The printpaths() traverse the tree using print_recurse() which assigns the code to each leaf node
 * character.
*/
	printpaths(st);
	i = 0;
	printf("...........\n file compression................\n");
	int pos = 0;
	fd = open(argv[1], O_RDONLY);    
	char *buffer = (char *)malloc(size);
	pos = 0;
/* It reads the file charater by character and store it in  a buffer.
 * Here the size is 1024, which can increase using realloc().
*/
	while(read(fd , &c, sizeof(char))){	
		buffer[pos++] = c;
		if(pos >= size - 1) { 
			size = size * 2;
			buffer = (char*)realloc(buffer, size);
		}
 	}
	buffer[pos] = '\0';
	tbuffer = (char*)malloc(size);
	strcpy(tbuffer,buffer);
	printf("Source Data In The File:\n");
	printf("%s\n",tbuffer);
	flen1 = strlen(tbuffer);
	close(fd);
	char *binary = (char*)malloc(size);
/* Now convert into binary form . Binary form is then  
* convert   into decimal and  assign the value ascii value to it ,then print character related to it,
* it so compress the data using decodefb().
*/
	convert(buffer,binary);
	char *coded;
	coded = (char*)malloc(size);
	size = strlen(binary) + 12;
	decodefb(binary, coded, size);   
	flen = strlen(coded);
	fd1 = open(gzname,O_WRONLY | O_CREAT ,S_IRUSR | S_IWUSR);
	write(fd1,coded,strlen(coded));
	printf("\ncompressed size: %d bytes\n",flen);
	printf("compressed file name:%s",gzname);
	close(fd1);
	free(buffer);
        free(binary);           
        free(coded);
	printf("\nDo YOU WANT TO UNCOMPRESSED THE FILE, (y/n) ENTER '1' for 'y' OR  '0' for 'n'..:\n");
	scanf("%d", &choice);
	if(choice){	
		strcpy(targv,argv[1]);
		m = 0;
		while(targv[m] != '.'){
				ctargv[m] = targv[m];
				m++;
		}
		strcat(ctargv,"_uncompress.txt");
		printf("Uncompressed Size: %d bytes\n",flen1);
		printf("Uncompressed File Name:");
		printf("%s", ctargv);
		fd=open(ctargv,O_WRONLY | O_CREAT , S_IRUSR | S_IWUSR);
		write(fd , tbuffer , strlen(tbuffer));
		close(fd);
	}		
	ratio = (float)flen / (float)flen1;
	printf("\ncompression ratio : %f\n", ratio);
	fd = open(gzname , O_RDONLY);
	close(fd);
	char* dele_name;
	int select;
	printf("\n-----------------------------------------------------------------------------------");
	printf("\n      Performing delete and concatenate operations on .gz   ");
	printf("\nDo you want to delete the file..*filename.gz*,Enter '0' for (n) OR '1'for(y):\n");
	scanf("%d",&select);
	if(select == 1)
		delete_file(gzname);
	else	
		printf("\nFile not deleted");
	char file1[14],file2[14];
	printf("\nDo you want to concatenate the .tar/.txt files '1' for (y) OR '0' for (n):\n");
	scanf("%d",&select);	
	if(select == 1)	{
		printf("Enter the two '.gz' FILE names to concatenate:\n");
		scanf("%s %s",file1 ,file2);
 		if(concate_file(file1,file2) != -1)
			printf("Concatenate successfully\n");
	}		
	else
		printf("File not concatanate:\n");
	printf("\n---------------------------------------------------------------------------------");
	printf("\n......Thank you.....\n"); 
	printf("\n---------------------------------------------------------------------------------\n");
}
int concate_file(char* file1,char* file2){
	int fd1,fd2,fd3;
	char ch;
	fd1 = open(file1,O_RDONLY);
	fd2 = open(file2,O_RDONLY);
	fd3 = open(file1,O_RDONLY |O_WRONLY | O_CREAT , S_IRUSR | S_IWUSR );
	if(fd1 == -1 || fd2 == -1 || fd3 == -1){
		perror("\nFiles not open successfully");
		errno = EINVAL;
		return -1;
	}
	while(read(fd1 , &ch , sizeof(char)))
		write(fd3 , &ch , sizeof(char));
	while(read(fd2 , &ch , sizeof(char)))
		write(fd3 , &ch , sizeof(char));
	return 0 ;
}
