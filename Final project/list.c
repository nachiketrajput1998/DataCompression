/* LIST as a ADT */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "list.h"
void init(list *l) {
	l->head = NULL;
	l->tail = NULL;
}
void init1(list1 *l) {
	l->head = NULL;
	l->tail = NULL;
}
void append(list *l, char ch){
	node *tmp, *temp;
	tmp = (node*)malloc(sizeof(node));
	tmp -> c = ch;
	tmp -> freq = 1;
	if(l -> head == NULL){
		l -> head = tmp;
		tmp -> next = NULL;
		l -> tail = tmp;
		return;
  	}
	else{
		temp = l -> tail;
		temp -> next = tmp;
		tmp -> next = NULL;
		l -> tail = tmp;
		return;
	}	
}
void search (list *l, char ch){
	int count = 0;
	node *tmp;
	tmp = l->head;
	while(tmp->c != ch){
		tmp = tmp->next;
		if (tmp == NULL){
			count = 1;
			break;
		}
	}
	if(count == 0){
		tmp -> freq = tmp -> freq + 1; 
	}
	if(count == 1){
		append(l, ch);
	}
}
void append1(list1 *l, char str[], char ch){
	int i = 0, j , k=0;
	data *tmp, *temp;
	tmp = (data*)malloc(sizeof(data));
	j = strlen(str);
	char c[j];
	while(str[i] != '\0'){
		if(str[i] == '1' || str[i] == '0')
			c[k++] = str[i];
		i++;				
	}
	c[k] = '\0';
	strcpy(tmp -> str,c);
	tmp -> ch = ch;
	if(l -> head == NULL){
		tmp -> next = NULL;
		l -> head = tmp;
		l -> tail = tmp;
		return;
	}
	else{
		temp = l->tail;
		temp -> next = tmp;
		tmp -> next = NULL;
		tmp -> prev = temp;
		l -> tail = tmp;
		return;
	}	
}


