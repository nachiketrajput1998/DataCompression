#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "list.h"
char ch, str[15];
void initial_wt(node *head){
	node *tmp;
	tmp = head;
	while(head != NULL){
		head -> weight = head -> freq;
		head -> left = NULL;
		head -> right = NULL;
		head = head -> next;
	}
	while(tmp != NULL){
		tmp = tmp -> next;
	}
}
int no_of_node(node *p){
	node* tmp;
	tmp = p;
        int count = 0;
	while(tmp != NULL){
		tmp = tmp -> next;
		count++;
	}
	return count;
}
node* bmerge(node *head){
	int m, i = 0 , n, j , k ;
	n  = no_of_node(head); 
	node *sort[n+1];
	while(i < n){	
		sort[i] = head;
		head = head->next;
		sort[i]->next = NULL;
		i++;
        }
	sort[i] = NULL;
	for ( m = n ; m > 1 ; m = ( m + 1) / 2){
		for ( j = 0, k = 0; k < m; j++){
			sort[j] = mergesort( sort[k] , sort[k+1] );
			k = k + 2 ;
		}
	sort[j] = NULL;    
	}
	return sort[0];
}
node* mergesort(node *sort1 , node* sort2){
	node *res = NULL;
	if(sort1 != NULL && sort2 != NULL){
		if(sort1 -> weight <= sort2 -> weight){
			res = sort1;
			res -> next = mergesort(sort1 -> next ,sort2);
		}
		else{
			res = sort2;
			res -> next = mergesort(sort1 , sort2 -> next);
		}
	}
	else{
		if(sort1 == NULL)
			return sort2;
		if(sort2 == NULL)
			return sort1;
	}
	return res;
}
node* huff_tree(node *head){
	int val1, val2, count = 1, check, prevwt;	
	node  *left_t , *right_t;
	node *temp, *tmp1,*net_wt, *start;
	while(head -> next != NULL){
		count = 1;
		left_t = head;
		val1 = left_t -> weight;
		head = head -> next;
		right_t = head;
		val2 = right_t -> weight;
		temp = (node*)malloc(sizeof(node));
		temp -> left = left_t;
		temp -> right = right_t;
		prevwt = val1 + val2;
		temp -> weight = prevwt;
		if (nhead == NULL){
			head = head -> next;
			nhead = temp;
			while(head != NULL){
				tmp1 = (node*)malloc(sizeof(node));
				tmp1 -> c = head -> c;
				tmp1 -> freq = head -> freq;
				tmp1 -> left = head -> left;
				tmp1 -> right = head -> right;
				tmp1 -> weight = head -> weight;
				tmp1 -> next = NULL;
				temp -> next = tmp1;
				head = head -> next;  
				temp = tmp1;           
			}
		}
		else{
			temp -> next = head -> next;
			nhead = temp;
		}
		head = bmerge(nhead);
	}
	return (head);
}
void printpaths(node *node){	
	char str[124];	
	extern list1 t; // to use the previously declared list, extern is used.
	init1(&t);
	print_recursive(t, node, str, ' ', 0);
	data *d;
	d = t.head;
	while(d != NULL){
		d = d->next;	
	}
}
void convert(char* buffer,  char* str){
	int size = 1024, len2, len1, state1 = 0, state0 = 0, check;
	int i = 0, j , k = 0;	        
	char ch;
	data *tmp;
	len2 = strlen(buffer);
	while(i < len2){
		ch = buffer[i];	
		tmp = t.head;
		while(tmp->ch != ch){
			tmp = tmp->next;
		}
		len1 = strlen(tmp->str);
		for(j = 0; j < len1; j++){
			str[k++] = (char) tmp->str[j];
			if(k >= size - 1) { 
				size = size * 2;
				str = (char*)realloc(str, size);
			}
			if( tmp->str[j] == '0'){
				state0++;
				if( state0 == 5){
					str[k++] = '1';
					state0 = 0; 
				}
				state1 = 0;
			}
			else{
				state0 = 0;
				state1++;
				if(state1 == 5){
					str[k++] = '0';
					state1 = 0; 
				}
			}
		}
		i++;
	}
	str[k] = '\0';
}
void decodefb(char* given, char* result,int size){
	int length , x = 0, y , a, b ,k = 0;
	length = strlen(given);
	int  count, ct, pow[8],check, rem , i = 0, pos = 0;
	int binary[8];    
	int ascii_value = 0;     
	char ascii;
	char  *ctgiven = (char *)malloc(size);      
	rem = 8 - (length + 3) % 8;
	if (rem == 8)
		rem = 0;
	while(i < rem){
		if(i % 2 == 1)
			strcat(given,"1");
		else
			strcat(given,"0");
		i++;
	}
	strcat(given,"\0");
	if(rem == 0){
		ctgiven[0] = '0';
		ctgiven[1] = '0'; 
		ctgiven[2] = '0';
	}
	else if(rem == 1){
		ctgiven[0] = '0';
		ctgiven[1] = '0'; 
		ctgiven[2] = '1';
	}
	else if(rem == 2){
		ctgiven[0] = '0';
		ctgiven[1] = '1'; 
		ctgiven[2] = '0';
	}
	else if(rem == 0){
		ctgiven[0] = '0';
		ctgiven[1] = '1'; 
		ctgiven[2] = '1';
	}
	else if(rem == 0){
		ctgiven[0] = '1';
		ctgiven[1] = '0'; 
		ctgiven[2] = '0';
	}
	else if(rem == 0){
		ctgiven[0] = '1';
		ctgiven[1] = '0'; 
		ctgiven[2] = '1';
	}
	else if(rem == 0){
		ctgiven[0] = '1';
		ctgiven[1] = '1'; 
		ctgiven[2] = '0';
	}
	else if(rem == 0){
		ctgiven[0] = '1';
		ctgiven[1] = '1'; 
		ctgiven[2] = '1';
	}	
	ctgiven[3] = '\0';
	strcat(ctgiven, given);
	given = (char*)realloc(given, size);        
	memset(given, '\0', sizeof(strlen(given)));
	strcpy(given, ctgiven);
	length = strlen(given);
	while(x < length / 8){
		for( a = 0; a < 8; a++ , k++){
			binary[a] = (int) given[k] - 48;    // char '1'-48 for integer 1 
		}
		count = 7;
		for( ct = 0; ct < 8; ct++ , count--){
			pow[ct] = count;      
		}
		for( y = 0; y < 8; y++){
			 a = binary[y];    
			 b = pow[y];    
			ascii_value = ascii_value + a * powb(b);  
		}
		ascii = ascii_value;   
		ascii_value = 0;    
		result[pos++] = (char)ascii;
		if(pos >= size - 1) { 
			size  = size * 2;
			result = (char*)realloc(result , size);
		}
		x++;
	}
	result[pos++] = '\0';
}
int powb(int n){
	int result = 1, i = 0;
	for(i = 0; i < n; i++)
		result = result * 2;
	return result;
}
void print_recursive(list1 t, node *node, char* str, char lr, int len){
	if (node == NULL) 
		return;
	str[len++] = lr;
	if (node->left == NULL && node->right == NULL){
		str[len] = '\0';
		appendstr(str, node->c);
	}
	else{
		print_recursive(t,node->left, str , '0', len);
		print_recursive(t,node->right, str ,'1', len);
	}
}
void appendstr(char* str, char ch){
	append1(&t, str, ch);
}    
void delete_file(char* dele_name){
		if (remove(dele_name) == 0)
    			 printf("Deleted successfully");
   		else
     			printf("Unable to delete the file");
	return ;
}	
char* uncompress(char* bs, int size){
	int i = 0,j = 0;
	char *tbs;
	int num ,rem ,base =1;
	int decimal_val = 0,binary_val = 0;
	tbs = (char*)malloc(sizeof(char *) * size);
	tbs = bs;
	while(bs[i] != '\0'){	
		tbs[j++] = bs[i++];
		tbs[j++] = bs[i++];
		tbs[j++] = bs[i++];
		tbs[j++] = '\0';
		printf("%s",tbs);
	}
	num = convert3(tbs);
		    binary_val = num;
		    while (num > 0){
		        rem = num % 10;
		        decimal_val = decimal_val + rem * base;	
		        num = num / 10 ;
		        base = base * 2;
		    }
	printf("%d",decimal_val);
	return 0;	
}
	
/* We have to take data in the file and convert it into ascii code if it is single chrater which is not *  repeated and make
/* a dictionary of new characters which starts from 256 */
int* convert_ascii(char buffer[],int size){
	int size1 = strlen(buffer);
	static int sb = 256;
	int *cbuffer,s ,change = 0, m = 0;
	cbuffer = (int*)malloc(sizeof(int)*size);	
	int i , j = 0 , k = 0;
	printf("%s",buffer);
	for(i = 0 ; i < size1 ; i++){
		//if(buffer[i] < buffer[i+1] && (int)buffer[i] < 256){
			cbuffer[j] = (int)buffer[i];
			j++;
	}
		for(j = 0 ; j < size1 ; j++)
		printf("%d\n",cbuffer[j]);
	
	return cbuffer;
}
int* remove_repeated(int a[],int n){
	int temp[n];
	int i = 0, j = 0, k = 0;
	while(i < n){
		for(j = 0; j < i ; j++){
			if(temp[j] == a[i]){
				break;
			}
		}	
		if(j == i ){
			temp[k] = a[i];
			k++;
		}	
		i++;
	}
	for(i = 0; i < k; i++){
		a[i] = temp[i];
	}
	return a;
}	
char* convert_binary(int a[],int size){
	long rem , base = 1 , no_of1 ,i = 0,num;
	char *c1binary, *cbinary;
	long  binary = 0;
	int count = 0;
	cbinary = (char*)malloc(sizeof(char)*size);
	c1binary = (char*)malloc(sizeof(char)*size);
	while(i < size){
    		num = a[i];
  		while (num > 0){
        		rem = num % 2;
        		if (rem == 1){
          			  no_of1++;
        		}
      			  binary = binary + rem * base;
      			  num = num / 2;
      			  base = base * 10;
    		}
	sprintf(c1binary,"%ld",binary);
	strcat(cbinary,c1binary);
	binary = 0;
	base = 1;
	i++;
	count++;
	}
	printf("\n%s",cbinary);
	return cbinary;
}
void swap(char *a,char*b){
	char t = *a;
	*a = *b;
	*b = t;
}
char *reverse(char *cp2,int i,int j){
	while(i < j)
		swap(&cp2[i++] , &cp2[j--]);
	return cp2;
}
char* itoac(int num , char* cp2 , int base){
	int n = abs(num);
	int i = 0, rem;
	while(n){
		rem = n % base;
		if(rem >= 10)
			cp2[i++] = 'A' + (rem -10);
		else
			cp2[i++] = '0' + rem;
		n = n / base;
	}
	if( i == 0)
		cp2[i++] = '\0';
	if(num < 0 && base == 10)
		cp2[i++] = '-';
	cp2[i] ='\0';
	return reverse(cp2,0,i-1);
}
	
int convert3(char* p2){
	char *cp2 = (char*)malloc(sizeof(int));
	static int i = 0,j = 0 ;
	int num ,rem ,base = 1;
	int decimal_val = 0,binary_val = 0;
	printf("\n%s",p2);
	while(p2[i] != '\0'){
		cp2[j++] =  p2[i];
		cp2[j++] =  p2[i++];
		cp2[j++] =  p2[i++];
		cp2[j++] =  '\0';
		j = 0;
		itoac(num , cp2 , 10);
		    binary_val = num;
		    while (num > 0){
		        rem = num % 10;
		        decimal_val = decimal_val + rem * base;	
		        num = num / 10 ;
		        base = base * 2;
		    }
		return decimal_val;	
	}
}



