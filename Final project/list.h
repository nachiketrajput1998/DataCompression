/* Typedef of LIST ADT */
typedef struct node{
	char c;
	int freq;
        int weight;
	struct node  *next, *prev;
	struct node  *left, *right;
}node;
typedef struct list{
	node *head, *tail;
}list;
typedef struct data{
	char str[16];
	char ch;
	struct data *next, *prev;
}data;
typedef struct list1{
  	 data *head, *tail;
}list1;
list1 t;
node *start,*nhead,*root, *s1, *start1, *head;
void  init(list*);
void  init1(list1*);
void  append(list*, char);
void  append1(list1*, char* str, char);
void  search(list*, char);
void  initial_wt(node *);
int   no_of_node(node*);
void  convert(char*, char*);
node* mergesort(node* ,node*);
node* bmerge(node *);
void  decodefb(char*, char* , int);
node* huff_tree(node*); 
int   powb(int);
void  printpaths(node* ); 
void  print_recursive(list1 , node* , char* , char, int); 
void  appendstr(char* str, char);
int   concate_file(char* ,char* );
void  delete_file(char* );
int   convert3(char* p2);
char* uncompress(char*, int);
char* itoac(int, char* , int);

