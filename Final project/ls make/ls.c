#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/dir.h>
#include <sys/param.h>

/* alphasort is a inbuild() use to sort the name of files in ascending order. 
*/

extern  int alphasort(); 
char pathname[MAXPATHLEN];
/* MAXPATHLEN is defined in a <sys/param.h> file
*/

int main(){
	int i , j;
/* The structure is defined in a <sys/dir.h> which include d_name array  of
 * undefined size.
 */
	struct direct**fnames;
/* getcwd gives the name of current working directory, which  is inbuild
*  in <unistd.h> file */

   	if(getcwd(pathname, sizeof(pathname)) != NULL)
    		printf("CWD :%s\n",pathname);
    	j = scandir(pathname, &fnames,NULL, alphasort);
    	if(j <= 0){
      		perror("Files not present in the directory\n");
    		errno = EINVAL;
		return errno;	
	}	
    	printf("Total no. of files in a PWD: %d\n",j);
    	for (i = 1; i < j + 1  ; ++i)
        	printf("%s  ",fnames[i-1]->d_name);
	    	
	printf("\n"); 
}

