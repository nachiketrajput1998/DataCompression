 Project Name : Data compression
 Name : Nachiket Rajendrasing Rajput.
 MIS : 111608054
 
 1. Firstly , I understand the concept of Huffman algorithm as , how it works.
 2. In this code, i had first made the linked list as ADT to perform the operation on it . I had use
    the doubly Null terminated linked list.
 3. I had first calculate the frequency of each  characters in the file using list ADT , read() reads the file character by character and calls append(). In append() the search() is called for searching the character in the list. If it is already present in the list then increment in the freq, otherwise append to the list.
 4 .After that  Huffman tree is created using the huff_tree(). With this function , now all the characters are the leaf nodes.
 5. Now ,for the requirement of binary string , the printpaths() is used which traverse the tree using 
  print_recursive() and assign the binary code to each character.The binary string is stored into the 	  another linked list using the appendstr(). 
 6 . Binary form is then convert into the decimal and assign the ascii value to it , then print the character related to it .for ex. if the string as "sta" has binary code using huffman tree is 01 101 110  so its decimal value is 156 and the character related to it is 'n' .So the data  is compressed.
 7. Then the compressed data is in .gz file and uncompressed data is in _uncompress.txt file.
 8 . I had also calculated the compression ratio and called some functios which ask to delete the particular file and concatanate two files.
 9. I had also written a code for 'ls command' and 'cat command' which helps to perform output operations effectively. The cat command is to see the data in uncompress.txt file ,and ls commnd gives the name of present working directory as well as list the files in ascending order.  
 7. I had also work on the implementation of lempel Ziv Welch algorithm. I had also written some functions related to it .But these functions are not called in a code. 
 8. In the entire project the memory operatios are handled using realloc() which calls when a size is greater than 1024 and the size become double.
 9. I avoid the segfault occuring in my code which is due to overwritting the string and more than one free() calls.
